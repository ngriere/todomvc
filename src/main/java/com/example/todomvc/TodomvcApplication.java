package com.example.todomvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.example.todomvc.adapter.database")
@SpringBootApplication
public class TodomvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(TodomvcApplication.class, args);
    }

}
