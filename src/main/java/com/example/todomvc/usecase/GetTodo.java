package com.example.todomvc.usecase;

import com.example.todomvc.controller.port.TodoGetUsecase;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.exceptions.TodoNotFoundException;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.UUID;

public class GetTodo implements TodoGetUsecase {
    private final TodoRepository todoRepository;
    private final TodoPresenter todoPresenter;

    public GetTodo(TodoRepository todoRepository, TodoPresenter todoPresenter) {
        this.todoRepository = todoRepository;
        this.todoPresenter = todoPresenter;
    }

    public TodoResult get(final UUID id) {
        try {
            final Todo todo = todoRepository.find(id).orElseThrow(() -> new TodoNotFoundException(id));
            return todoPresenter.present(todo);
        } catch (TodoNotFoundException ex) {
            return todoPresenter.notFound(id, ex);
        }
    }
}
