package com.example.todomvc.usecase;

import com.example.todomvc.controller.model.TodoPartialUpdate;
import com.example.todomvc.controller.model.TodoUpdate;
import com.example.todomvc.controller.port.TodoUpdateUsecase;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.exceptions.TodoConflictException;
import com.example.todomvc.usecase.exceptions.TodoNotFoundException;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.UUID;

public class UpdateTodo implements TodoUpdateUsecase {

    private final TodoRepository todoRepository;
    private final TodoPresenter todoPresenter;

    public UpdateTodo(TodoRepository todoRepository, TodoPresenter todoPresenter) {
        this.todoRepository = todoRepository;
        this.todoPresenter = todoPresenter;
    }

    public TodoResult update(final UUID id, final TodoUpdate todoUpdate) {
        try {
            final Todo todoToUpdate = findTodoToUpdate(id);
            checkIfConflictingOrder(todoToUpdate, todoUpdate.order());
            final Todo updatedTodo = new Todo(todoToUpdate.id(), todoUpdate.title(), todoUpdate.completed(), todoUpdate.order());
            todoRepository.save(updatedTodo);
            return todoPresenter.present(updatedTodo);
        } catch (IllegalArgumentException ex) {
            return todoPresenter.invalid(ex);
        } catch (TodoNotFoundException todoNotFoundException) {
            return todoPresenter.notFound(id, todoNotFoundException);
        } catch (TodoConflictException todoConflictException) {
            return todoPresenter.conflict(id, todoConflictException);
        }
    }

    private Todo findTodoToUpdate(UUID id) throws TodoNotFoundException {
        return todoRepository.find(id).orElseThrow(() -> new TodoNotFoundException(id));
    }

    private void checkIfConflictingOrder(Todo todoToUpdate, long order) throws TodoConflictException {
        final boolean todoExistsWithThatOrder = todoRepository.findAll().stream()
                .anyMatch(todo -> !todo.equals(todoToUpdate) && todo.order() == order);

        if (todoExistsWithThatOrder) {
            throw new TodoConflictException(order);
        }
    }

    public TodoResult update(final UUID id, final TodoPartialUpdate todoPartialUpdate) {
        try {
            final Todo todoToUpdate = findTodoToUpdate(id);
            if (todoPartialUpdate.order().isPresent()) {
                checkIfConflictingOrder(todoToUpdate, todoPartialUpdate.order().get());
            }
            final Todo updatedTodo = buildUpdatedTodo(todoPartialUpdate, todoToUpdate);
            todoRepository.save(updatedTodo);
            return todoPresenter.present(updatedTodo);
        } catch (IllegalArgumentException ex) {
            return todoPresenter.invalid(ex);
        } catch (TodoNotFoundException todoNotFoundException) {
            return todoPresenter.notFound(id, todoNotFoundException);
        } catch (TodoConflictException todoConflictException) {
            return todoPresenter.conflict(id, todoConflictException);
        }
    }

    private Todo buildUpdatedTodo(TodoPartialUpdate todoPartialUpdate, Todo todoToUpdate) {
        final String title = todoPartialUpdate.title().orElse(todoToUpdate.title());
        final boolean completed = todoPartialUpdate.completed().orElse(todoToUpdate.completed());
        final long order = todoPartialUpdate.order().orElse(todoToUpdate.order());
        return new Todo(todoToUpdate.id(), title, completed, order);
    }
}
