package com.example.todomvc.usecase.exceptions;

import java.util.UUID;

public class TodoNotFoundException extends Exception {
    static final String MESSAGE_PATTERN = "No todo with id %s, was found";

    public TodoNotFoundException(UUID todoId) {
        super(String.format(MESSAGE_PATTERN, todoId.toString()));
    }
}
