package com.example.todomvc.usecase.exceptions;

public class TodoConflictException extends Exception {
    static final String MESSAGE_PATTERN = "A conflicting todo with order id %d, was found";

    public TodoConflictException(long order) {
        super(String.format(MESSAGE_PATTERN, order));
    }
}
