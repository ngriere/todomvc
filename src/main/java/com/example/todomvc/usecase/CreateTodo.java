package com.example.todomvc.usecase;

import com.example.todomvc.controller.model.TodoCreation;
import com.example.todomvc.controller.port.TodoCreateUsecase;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.UUID;

public class CreateTodo implements TodoCreateUsecase {

    private final TodoRepository todoRepository;
    private final TodoPresenter todoPresenter;

    public CreateTodo(TodoRepository todoRepository, TodoPresenter todoPresenter) {
        this.todoRepository = todoRepository;
        this.todoPresenter = todoPresenter;
    }

    public TodoResult create(final TodoCreation todoCreation) {
        try {
            final Todo todo = todoRepository.save(new Todo(UUID.randomUUID(), todoCreation.title(), false, todoRepository.count() + 1));
            return todoPresenter.present(todo);
        } catch (IllegalArgumentException ex) {
            return todoPresenter.invalid(ex);
        }
    }
}
