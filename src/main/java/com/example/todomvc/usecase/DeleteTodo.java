package com.example.todomvc.usecase;

import com.example.todomvc.controller.port.TodoDeleteUsecase;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.usecase.exceptions.TodoNotFoundException;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.UUID;

public class DeleteTodo implements TodoDeleteUsecase {
    private final TodoRepository todoRepository;
    private final TodoPresenter todoPresenter;

    public DeleteTodo(TodoRepository todoRepository, TodoPresenter todoPresenter) {
        this.todoRepository = todoRepository;
        this.todoPresenter = todoPresenter;
    }

    public void delete(final UUID id) {
        try {
            final Todo todo = todoRepository.find(id).orElseThrow(() -> new TodoNotFoundException(id));
            todoRepository.remove(todo);
        } catch (TodoNotFoundException ex) {
            todoPresenter.notFound(id, ex);
        }
    }
}
