package com.example.todomvc.usecase;

import com.example.todomvc.controller.port.TodoListUsecase;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.List;

public class ListTodos implements TodoListUsecase {

    private final TodoRepository todoRepository;
    private final TodoPresenter todoPresenter;

    public ListTodos(TodoRepository todoRepository, TodoPresenter todoPresenter) {
        this.todoRepository = todoRepository;
        this.todoPresenter = todoPresenter;
    }

    public List<TodoResult> list() {
        final List<Todo> todos = todoRepository.findAll();
        return todoPresenter.present(todos);
    }
}
