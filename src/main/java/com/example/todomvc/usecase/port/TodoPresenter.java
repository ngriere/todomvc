package com.example.todomvc.usecase.port;

import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.exceptions.TodoConflictException;
import com.example.todomvc.usecase.exceptions.TodoNotFoundException;

import java.util.List;
import java.util.UUID;

public interface TodoPresenter {
    TodoResult present(Todo todo);

    List<TodoResult> present(List<Todo> todos);

    TodoResult invalid(IllegalArgumentException ex);

    TodoResult notFound(UUID id, TodoNotFoundException ex);

    TodoResult conflict(UUID id, TodoConflictException ex);
}