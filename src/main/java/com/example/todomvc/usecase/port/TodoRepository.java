package com.example.todomvc.usecase.port;

import com.example.todomvc.domain.Todo;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TodoRepository {

    long count();

    Todo save(Todo todo);

    Optional<Todo> find(UUID id);

    List<Todo> findAll();

    void remove(Todo todo);

    List<Todo> findCompleted();
}
