package com.example.todomvc.usecase;

import com.example.todomvc.controller.port.TodoDeleteAllUsecase;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.List;

public class DeleteAllTodos implements TodoDeleteAllUsecase {
    private final TodoRepository todoRepository;

    public DeleteAllTodos(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public void deleteAll(final boolean completed) {
        List<Todo> todos = completed ? todoRepository.findCompleted() : todoRepository.findAll();
        todos.forEach(todoRepository::remove);
    }
}
