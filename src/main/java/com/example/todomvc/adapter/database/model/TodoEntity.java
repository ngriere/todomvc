package com.example.todomvc.adapter.database.model;

import com.example.todomvc.domain.Todo;
import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;

import java.util.UUID;

@Entity
@Table(name = "TODO")
public class TodoEntity {
    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", updatable = false, nullable = false)
    @ColumnDefault("random_uuid()")
    private UUID id;

    @Column(name = "title", length = 64, nullable = false)
    private String title;

    @Column(name = "completed", nullable = false)
    private boolean completed;

    @Column(name = "todo_order", nullable = false)
    private long order;

    public static TodoEntity toTodoEntity(final Todo todo) {
        final TodoEntity todoEntity = new TodoEntity();
        todoEntity.id = todo.id();
        todoEntity.title = todo.title();
        todoEntity.completed = todo.completed();
        todoEntity.order = todo.order();
        return todoEntity;
    }

    public Todo toTodo() {
        return new Todo(id, title, completed, order);
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public long getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
