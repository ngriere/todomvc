package com.example.todomvc.adapter.database;

import com.example.todomvc.adapter.database.model.TodoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface TodoDao extends JpaRepository<TodoEntity, UUID> {
}
