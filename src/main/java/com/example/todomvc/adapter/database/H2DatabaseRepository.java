package com.example.todomvc.adapter.database;

import com.example.todomvc.adapter.database.model.TodoEntity;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class H2DatabaseRepository implements TodoRepository {

    private final TodoDao todoDao;

    public H2DatabaseRepository(TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    @Override
    public long count() {
        return todoDao.count();
    }

    @Override
    public Todo save(Todo todo) {
        final TodoEntity todoEntity = todoDao.save(TodoEntity.toTodoEntity(todo));
        return todoEntity.toTodo();
    }

    @Override
    public Optional<Todo> find(UUID id) {
        return todoDao.findById(id).map(TodoEntity::toTodo);
    }

    @Override
    public List<Todo> findAll() {
        return todoDao.findAll().stream().map(TodoEntity::toTodo)
                .sorted(Comparator.comparingLong(Todo::order)).toList();
    }

    @Override
    public void remove(Todo todo) {
        final TodoEntity todoEntity = TodoEntity.toTodoEntity(todo);
        todoDao.delete(todoEntity);
    }

    @Override
    public List<Todo> findCompleted() {
        return todoDao.findAll().stream().map(TodoEntity::toTodo).filter(Todo::completed).toList();
    }
}
