package com.example.todomvc.domain;

import java.util.UUID;

public record Todo(UUID id, String title, boolean completed, long order) {
    public Todo {
        if (title.isEmpty() || title.isBlank()) {
            throw new IllegalArgumentException("Invalid title");
        }

        if (order < 1) {
            throw new IllegalArgumentException("Invalid order");
        }
    }
}
