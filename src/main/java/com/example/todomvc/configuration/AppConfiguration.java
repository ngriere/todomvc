package com.example.todomvc.configuration;

import com.example.todomvc.adapter.database.H2DatabaseRepository;
import com.example.todomvc.adapter.database.TodoDao;
import com.example.todomvc.controller.port.*;
import com.example.todomvc.presenter.TodoWebPresenter;
import com.example.todomvc.usecase.*;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    private final TodoDao todoDao;

    public AppConfiguration(TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    @Bean
    public TodoRepository todoRepository() {
        return new H2DatabaseRepository(todoDao);
    }

    @Bean
    public TodoPresenter todoPresenter() {
        return new TodoWebPresenter();
    }

    @Bean
    public TodoCreateUsecase todoCreateUsecase() {
        return new CreateTodo(todoRepository(), todoPresenter());
    }

    @Bean
    public TodoListUsecase todoListUsecase() {
        return new ListTodos(todoRepository(), todoPresenter());
    }

    @Bean
    public TodoDeleteAllUsecase todoDeleteAllUsecase() {
        return new DeleteAllTodos(todoRepository());
    }

    @Bean
    public TodoGetUsecase todoGetUsecase() {
        return new GetTodo(todoRepository(), todoPresenter());
    }

    @Bean
    public TodoUpdateUsecase todoUpdateUsecase() {
        return new UpdateTodo(todoRepository(), todoPresenter());
    }

    @Bean
    public TodoDeleteUsecase todoDeleteUsecase() {
        return new DeleteTodo(todoRepository(), todoPresenter());
    }
}
