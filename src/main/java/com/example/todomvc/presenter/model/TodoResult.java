package com.example.todomvc.presenter.model;

import com.example.todomvc.domain.Todo;

import java.util.UUID;

public record TodoResult(UUID id, String title, boolean completed, long order, String url) {

    public static TodoResult toTodoResult(final Todo todo) {
        return new TodoResult(todo.id(), todo.title(), todo.completed(), todo.order(), "http://localhost:8080/todos/" + todo.id());
    }
}