package com.example.todomvc.presenter;

import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.exceptions.TodoConflictException;
import com.example.todomvc.usecase.exceptions.TodoNotFoundException;
import com.example.todomvc.usecase.port.TodoPresenter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

public class TodoWebPresenter implements TodoPresenter {
    static final String TODO_INVALID_MESSAGE = "Invalid todo supplied";
    static final String TODO_NOT_FOUND_MESSAGE = "No todo with id %s, was found";
    static final String CONFLICTING_TODO_MESSAGE = "A conflicting todo with order id %s, was found";

    @Override
    public TodoResult present(Todo todo) {
        return TodoResult.toTodoResult(todo);
    }

    @Override
    public List<TodoResult> present(List<Todo> todos) {
        return todos.stream().map(TodoResult::toTodoResult).toList();
    }

    @Override
    public TodoResult invalid(IllegalArgumentException ex) {
        throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, TODO_INVALID_MESSAGE, ex);
    }

    @Override
    public TodoResult notFound(UUID id, TodoNotFoundException ex) {
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, String.format(TODO_NOT_FOUND_MESSAGE, id), ex);
    }

    @Override
    public TodoResult conflict(UUID id, TodoConflictException ex) {
        throw new ResponseStatusException(
                HttpStatus.CONFLICT, String.format(CONFLICTING_TODO_MESSAGE, id), ex);
    }
}
