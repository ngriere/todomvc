package com.example.todomvc.controller.port;

import com.example.todomvc.presenter.model.TodoResult;

import java.util.List;

public interface TodoListUsecase {
    List<TodoResult> list();
}
