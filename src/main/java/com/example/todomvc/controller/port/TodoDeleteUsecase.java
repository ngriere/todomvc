package com.example.todomvc.controller.port;

import java.util.UUID;

public interface TodoDeleteUsecase {
    void delete(UUID id);
}
