package com.example.todomvc.controller.port;

import com.example.todomvc.controller.model.TodoPartialUpdate;
import com.example.todomvc.controller.model.TodoUpdate;
import com.example.todomvc.presenter.model.TodoResult;

import java.util.UUID;

public interface TodoUpdateUsecase {
    TodoResult update(final UUID id, final TodoUpdate todoUpdate);

    TodoResult update(final UUID id, final TodoPartialUpdate todoPartialUpdate);
}
