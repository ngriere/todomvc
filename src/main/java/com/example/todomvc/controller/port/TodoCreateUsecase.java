package com.example.todomvc.controller.port;

import com.example.todomvc.controller.model.TodoCreation;
import com.example.todomvc.presenter.model.TodoResult;

public interface TodoCreateUsecase {
    TodoResult create(TodoCreation todoCreation);
}
