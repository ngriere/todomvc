package com.example.todomvc.controller.port;

public interface TodoDeleteAllUsecase {
    void deleteAll(boolean completed);
}
