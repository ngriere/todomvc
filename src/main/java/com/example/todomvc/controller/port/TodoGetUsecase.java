package com.example.todomvc.controller.port;

import com.example.todomvc.presenter.model.TodoResult;

import java.util.UUID;

public interface TodoGetUsecase {
    TodoResult get(final UUID id);
}
