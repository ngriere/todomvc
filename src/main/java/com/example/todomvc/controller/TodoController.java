package com.example.todomvc.controller;

import com.example.todomvc.controller.model.TodoPartialUpdate;
import com.example.todomvc.controller.model.TodoUpdate;
import com.example.todomvc.controller.port.TodoDeleteUsecase;
import com.example.todomvc.controller.port.TodoGetUsecase;
import com.example.todomvc.controller.port.TodoUpdateUsecase;
import com.example.todomvc.presenter.model.TodoResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("todos/{id}")
class TodoController {
    private final TodoGetUsecase todoGetUsecase;
    private final TodoUpdateUsecase todoUpdateUsecase;
    private final TodoDeleteUsecase todoDeleteUsecase;

    TodoController(TodoGetUsecase todoGetUsecase, TodoUpdateUsecase todoUpdateUsecase, TodoDeleteUsecase todoDeleteUsecase) {
        this.todoGetUsecase = todoGetUsecase;
        this.todoUpdateUsecase = todoUpdateUsecase;
        this.todoDeleteUsecase = todoDeleteUsecase;
    }

    @GetMapping
    ResponseEntity<TodoResult> get(@PathVariable("id") UUID id) {
        return ResponseEntity.ok(todoGetUsecase.get(id));
    }

    @PutMapping
    ResponseEntity<TodoResult> update(@PathVariable("id") UUID id, @RequestBody TodoUpdate todoUpdate) {
        return ResponseEntity.ok(todoUpdateUsecase.update(id, todoUpdate));
    }

    @PatchMapping
    ResponseEntity<TodoResult> update(@PathVariable("id") UUID id, @RequestBody TodoPartialUpdate todoPartialUpdate) {
        return ResponseEntity.ok(todoUpdateUsecase.update(id, todoPartialUpdate));
    }

    @DeleteMapping
    ResponseEntity<String> delete(@PathVariable("id") UUID id) {
        todoDeleteUsecase.delete(id);
        return ResponseEntity.noContent().build();
    }
}
