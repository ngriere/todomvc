package com.example.todomvc.controller.model;

public record TodoUpdate(String title, boolean completed, int order) {
}
