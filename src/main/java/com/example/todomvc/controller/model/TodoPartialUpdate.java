package com.example.todomvc.controller.model;

import java.util.Optional;

public record TodoPartialUpdate(Optional<String> title, Optional<Boolean> completed, Optional<Long> order) {
}
