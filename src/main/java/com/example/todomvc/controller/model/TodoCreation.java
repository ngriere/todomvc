package com.example.todomvc.controller.model;

public record TodoCreation(String title) {
}