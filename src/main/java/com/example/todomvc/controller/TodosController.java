package com.example.todomvc.controller;

import com.example.todomvc.controller.model.TodoCreation;
import com.example.todomvc.controller.port.TodoCreateUsecase;
import com.example.todomvc.controller.port.TodoDeleteAllUsecase;
import com.example.todomvc.controller.port.TodoListUsecase;
import com.example.todomvc.presenter.model.TodoResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("todos")
class TodosController {

    private final TodoCreateUsecase todoCreateUsecase;
    private final TodoListUsecase todoListUsecase;
    private final TodoDeleteAllUsecase todoDeleteAllUsecase;

    TodosController(TodoCreateUsecase todoCreateUsecase, TodoListUsecase todoListUsecase,
                    TodoDeleteAllUsecase todoDeleteAllUsecase) {
        this.todoCreateUsecase = todoCreateUsecase;
        this.todoListUsecase = todoListUsecase;
        this.todoDeleteAllUsecase = todoDeleteAllUsecase;
    }

    @PostMapping
    ResponseEntity<TodoResult> create(@RequestBody TodoCreation todoCreation) {
        TodoResult todoResult = todoCreateUsecase.create(todoCreation);
        return ResponseEntity.status(HttpStatus.CREATED).body(todoResult);
    }

    @GetMapping
    ResponseEntity<List<TodoResult>> list() {
        return ResponseEntity.ok(todoListUsecase.list());
    }

    @DeleteMapping
    ResponseEntity<String> deleteAll(@RequestParam(required = false, defaultValue = "false") boolean completed) {
        todoDeleteAllUsecase.deleteAll(completed);
        return ResponseEntity.noContent().build();
    }
}
