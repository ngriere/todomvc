package com.example.todomvc.adapter;

import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.exceptions.TodoConflictException;
import com.example.todomvc.usecase.exceptions.TodoNotFoundException;
import com.example.todomvc.usecase.port.TodoPresenter;

import java.util.List;
import java.util.UUID;

public class InMemoryTodoPresenter implements TodoPresenter {
    private boolean invalid = false;
    private boolean notFound = false;
    private boolean conflict = false;

    @Override
    public TodoResult present(Todo todo) {
        return TodoResult.toTodoResult(todo);
    }

    @Override
    public List<TodoResult> present(List<Todo> todos) {
        return todos.stream().map(TodoResult::toTodoResult).toList();
    }

    @Override
    public TodoResult invalid(IllegalArgumentException ex) {
        invalid = true;
        return null;
    }

    @Override
    public TodoResult notFound(UUID id, TodoNotFoundException ex) {
        notFound = true;
        return null;
    }

    @Override
    public TodoResult conflict(UUID id, TodoConflictException ex) {
        conflict = true;
        return null;
    }

    public void clean() {
        invalid = false;
        conflict = false;
        notFound = false;
    }

    public boolean isNotFound() {
        return notFound;
    }

    public boolean isConflict() {
        return conflict;
    }

    public boolean isInvalid() {
        return invalid;
    }
}
