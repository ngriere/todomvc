package com.example.todomvc.adapter;

import com.example.todomvc.domain.Todo;
import com.example.todomvc.usecase.port.TodoRepository;

import java.util.*;

public class InMemoryTodoRepository implements TodoRepository {

    private final Map<UUID, Todo> todos = new HashMap<>();

    @Override
    public long count() {
        return todos.size();
    }

    @Override
    public Todo save(Todo todo) {
        todos.put(todo.id(), todo);
        return todo;
    }

    @Override
    public Optional<Todo> find(UUID id) {
        return Optional.ofNullable(todos.get(id));
    }

    @Override
    public List<Todo> findAll() {
        return todos.values().stream().sorted(Comparator.comparingLong(Todo::order)).toList();
    }

    @Override
    public void remove(Todo todo) {
        todos.remove(todo.id());
    }

    @Override
    public List<Todo> findCompleted() {
        return todos.values().stream().filter(Todo::completed).toList();
    }
}
