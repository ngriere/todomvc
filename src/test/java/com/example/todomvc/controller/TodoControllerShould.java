package com.example.todomvc.controller;

import com.example.todomvc.controller.model.TodoPartialUpdate;
import com.example.todomvc.controller.model.TodoUpdate;
import com.example.todomvc.controller.port.TodoDeleteUsecase;
import com.example.todomvc.controller.port.TodoGetUsecase;
import com.example.todomvc.controller.port.TodoUpdateUsecase;
import com.example.todomvc.presenter.model.TodoResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class TodoControllerShould {

    private TodoGetUsecase todoGetUsecase;
    private TodoUpdateUsecase todoUpdateUsecase;
    private TodoDeleteUsecase todoDeleteUsecase;
    private TodoController todoController;

    @BeforeEach
    public void setUp() {
        todoGetUsecase = mock(TodoGetUsecase.class);
        todoUpdateUsecase = mock(TodoUpdateUsecase.class);
        todoDeleteUsecase = mock(TodoDeleteUsecase.class);
        todoController = new TodoController(todoGetUsecase, todoUpdateUsecase, todoDeleteUsecase);
    }

    @Test
    @DisplayName("Get a todo")
    void get_a_todo() {
        // GIVEN
        final UUID id = UUID.fromString("c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoResult todoResult = new TodoResult(id, "Do Laundry",
                false, 1, "http://localhost:8080/todos/c5e0874f-fec2-40c6-9118-3469e4e2f403");
        when(todoGetUsecase.get(id)).thenReturn(todoResult);

        // WHEN
        final ResponseEntity<TodoResult> todoResultResponseEntity = todoController.get(id);

        // THEN
        assertThat(todoResultResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(todoResultResponseEntity.getBody()).isEqualTo(todoResult);
    }

    @Test
    @DisplayName("Update a todo")
    void update_a_todo() {
        // GIVEN
        final UUID id = UUID.fromString("c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoResult todoResult = new TodoResult(id, "Do shopping",
                false, 1, "http://localhost:8080/todos/c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoUpdate todoUpdate = new TodoUpdate("Do shopping", true, 1);
        when(todoUpdateUsecase.update(id, todoUpdate)).thenReturn(todoResult);

        // WHEN
        final ResponseEntity<TodoResult> todoResultResponseEntity = todoController.update(id, todoUpdate);

        // THEN
        assertThat(todoResultResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(todoResultResponseEntity.getBody()).isEqualTo(todoResult);
    }

    @Test
    @DisplayName("Update a todo partially")
    void update_a_todo_partially() {
        // GIVEN
        final UUID id = UUID.fromString("c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoResult todoResult = new TodoResult(id, "Do shopping",
                false, 1, "http://localhost:8080/todos/c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoPartialUpdate todoPartialUpdate = new TodoPartialUpdate(Optional.of("Do shopping"), Optional.empty(), Optional.empty());
        when(todoUpdateUsecase.update(id, todoPartialUpdate)).thenReturn(todoResult);

        // WHEN
        final ResponseEntity<TodoResult> todoResultResponseEntity = todoController.update(id, todoPartialUpdate);

        // THEN
        assertThat(todoResultResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(todoResultResponseEntity.getBody()).isEqualTo(todoResult);
    }

    @Test
    @DisplayName("Delete a todo")
    void delete_a_todo() {
        // GIVEN
        final UUID id = UUID.fromString("c5e0874f-fec2-40c6-9118-3469e4e2f403");
        doNothing().when(todoDeleteUsecase).delete(id);

        // WHEN
        final ResponseEntity<String> responseEntity = todoController.delete(id);

        // THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
        assertThat(responseEntity.getBody()).isNull();
    }
}