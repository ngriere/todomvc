package com.example.todomvc.controller;

import com.example.todomvc.controller.model.TodoCreation;
import com.example.todomvc.controller.port.TodoCreateUsecase;
import com.example.todomvc.controller.port.TodoDeleteAllUsecase;
import com.example.todomvc.controller.port.TodoListUsecase;
import com.example.todomvc.presenter.model.TodoResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class TodosControllerShould {

    private TodoCreateUsecase todoCreateUsecase;
    private TodoListUsecase todoListUsecase;
    private TodoDeleteAllUsecase todoDeleteAllUsecase;
    private TodosController todosController;

    @BeforeEach
    public void setUp() {
        todoCreateUsecase = mock(TodoCreateUsecase.class);
        todoListUsecase = mock(TodoListUsecase.class);
        todoDeleteAllUsecase = mock(TodoDeleteAllUsecase.class);
        todosController = new TodosController(todoCreateUsecase, todoListUsecase, todoDeleteAllUsecase);
    }

    @Test
    @DisplayName("Create a todo")
    void create_a_todo() {
        // GIVEN
        final UUID id = UUID.fromString("c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoResult todoResult = new TodoResult(id, "Do Laundry",
                false, 1, "http://localhost:8080/todos/c5e0874f-fec2-40c6-9118-3469e4e2f403");
        final TodoCreation todoCreation = new TodoCreation("Do Laundry");

        when(todoCreateUsecase.create(todoCreation)).thenReturn(todoResult);

        // WHEN
        final ResponseEntity<TodoResult> todoResultResponseEntity = todosController.create(todoCreation);

        // THEN
        assertThat(todoResultResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(todoResultResponseEntity.getBody()).isEqualTo(todoResult);
    }

    @Test
    @DisplayName("List all todos")
    void list_all_todos() {
        // GIVEN
        final TodoResult todoResult1 = new TodoResult(UUID.randomUUID(), "Do Laundry",
                false, 1, "http://localhost:8080/todos/1");
        final TodoResult todoResult2 = new TodoResult(UUID.randomUUID(), "Do Dishes",
                true, 2, "http://localhost:8080/todos/1");
        final List<TodoResult> todoResults = new ArrayList<>();
        todoResults.add(todoResult1);
        todoResults.add(todoResult2);
        when(todoListUsecase.list()).thenReturn(todoResults);

        // WHEN
        final ResponseEntity<List<TodoResult>> todoResultResponseEntity = todosController.list();

        // THEN
        assertThat(todoResultResponseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(todoResultResponseEntity.getBody()).hasSize(2);

    }

    @Test
    @DisplayName("Delete all todos")
    void delete_all_todos() {
        // GIVEN
        doNothing().when(todoDeleteAllUsecase).deleteAll(false);

        // WHEN
        final ResponseEntity<String> todoResultResponseEntity = todosController.deleteAll(false);

        // THEN
        assertThat(todoResultResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }
}