package com.example.todomvc.usecase;

import com.example.todomvc.adapter.InMemoryTodoPresenter;
import com.example.todomvc.adapter.InMemoryTodoRepository;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.port.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class GetTodoShould {
    private TodoRepository todoRepository;
    private InMemoryTodoPresenter todoPresenter;
    private GetTodo getTodo;

    @BeforeEach
    public void setUp() {
        todoRepository = new InMemoryTodoRepository();
        todoPresenter = new InMemoryTodoPresenter();
        getTodo = new GetTodo(todoRepository, todoPresenter);
        todoPresenter.clean();
    }

    @Test
    @DisplayName("get a todo")
    void get_todo() {
        // GIVEN
        final UUID id = UUID.randomUUID();
        final Todo todo = new Todo(id, "Do Laundry", false, 1);
        todoRepository.save(todo);

        // WHEN
        final TodoResult actualTodo = getTodo.get(id);

        // THEN
        assertThat(actualTodo.id()).isEqualTo(todo.id());
    }

    @Test
    @DisplayName("handle non-existing todo")
    void handle_non_existing_todo() {
        // GIVEN
        final UUID id = UUID.randomUUID();

        // WHEN
        final TodoResult actualTodo = getTodo.get(id);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isNotFound()).isTrue();
    }
}