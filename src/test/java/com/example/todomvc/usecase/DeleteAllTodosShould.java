package com.example.todomvc.usecase;

import com.example.todomvc.adapter.InMemoryTodoRepository;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.usecase.port.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class DeleteAllTodosShould {

    private TodoRepository todoRepository;
    private DeleteAllTodos deleteAllTodos;

    @BeforeEach
    public void setUp() {
        todoRepository = new InMemoryTodoRepository();
        deleteAllTodos = new DeleteAllTodos(todoRepository);
        initialiseTodos();
    }

    private void initialiseTodos() {
        final Todo todo1 = new Todo(UUID.randomUUID(), "Do laundry", false, 1);
        final Todo todo2 = new Todo(UUID.randomUUID(), "Do dishes", true, 2);
        final Todo todo3 = new Todo(UUID.randomUUID(), "Do cleaning", true, 3);
        final Todo todo4 = new Todo(UUID.randomUUID(), "Do shopping", false, 4);
        todoRepository.save(todo1);
        todoRepository.save(todo2);
        todoRepository.save(todo3);
        todoRepository.save(todo4);
    }

    @Test
    @DisplayName("delete all todos")
    void delete_all_todos() {
        // GIVEN
        assertThat(todoRepository.findAll()).hasSize(4);

        // WHEN
        deleteAllTodos.deleteAll(false);

        // THEN
        assertThat(todoRepository.findAll()).isEmpty();
    }

    @Test
    @DisplayName("delete all completed todos")
    void delete_all_completed_todos() {
        // GIVEN
        assertThat(todoRepository.findCompleted()).hasSize(2);

        // WHEN
        deleteAllTodos.deleteAll(true);

        // THEN
        assertThat(todoRepository.findCompleted()).isEmpty();
    }

}