package com.example.todomvc.usecase;

import com.example.todomvc.adapter.InMemoryTodoPresenter;
import com.example.todomvc.adapter.InMemoryTodoRepository;
import com.example.todomvc.controller.model.TodoCreation;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.port.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CreateTodoShould {

    private TodoRepository todoRepository;
    private InMemoryTodoPresenter todoPresenter;
    private CreateTodo createTodo;

    @BeforeEach
    public void setUp() {
        todoRepository = new InMemoryTodoRepository();
        todoPresenter = new InMemoryTodoPresenter();
        createTodo = new CreateTodo(todoRepository, todoPresenter);
        todoPresenter.clean();
    }

    @Test
    @DisplayName("create a todo")
    void create_todo() {
        // GIVEN
        final String title = "Do laundry";
        final TodoCreation todoCreation = new TodoCreation(title);

        // WHEN
        final TodoResult todoResult = createTodo.create(todoCreation);

        // THEN
        assertThat(todoResult.id()).isNotNull();
        assertThat(todoResult.title()).isEqualTo(title);
        assertThat(todoResult.order()).isEqualTo(1);
        assertThat(todoResult.completed()).isFalse();
        assertThat(todoResult.url()).isNotNull();
    }

    @Test
    @DisplayName("reject creating a todo with an invalid title")
    void reject_todo_with_invalid_title() {
        // GIVEN
        final String title = " ";
        final TodoCreation todoCreation = new TodoCreation(title);

        // WHEN
        final TodoResult todoResult = createTodo.create(todoCreation);

        // THEN
        assertThat(todoResult).isNull();
        assertThat(todoPresenter.isInvalid()).isTrue();
    }
}