package com.example.todomvc.usecase;

import com.example.todomvc.adapter.InMemoryTodoPresenter;
import com.example.todomvc.adapter.InMemoryTodoRepository;
import com.example.todomvc.controller.model.TodoPartialUpdate;
import com.example.todomvc.controller.model.TodoUpdate;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.port.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class UpdateTodoShould {

    private TodoRepository todoRepository;
    private InMemoryTodoPresenter todoPresenter;
    private UpdateTodo updateTodo;

    @BeforeEach
    public void setUp() {
        todoRepository = new InMemoryTodoRepository();
        todoPresenter = new InMemoryTodoPresenter();
        updateTodo = new UpdateTodo(todoRepository, todoPresenter);
        todoPresenter.clean();
    }

    @Test
    @DisplayName("update a todo completely")
    void update_todo() {
        // GIVEN
        final UUID id = UUID.randomUUID();
        final Todo todo = new Todo(id, "Do Laundry", false, 1);
        todoRepository.save(todo);

        // WHEN
        final TodoUpdate todoUpdate = new TodoUpdate("Do shopping", true, 1);
        final TodoResult result = updateTodo.update(id, todoUpdate);

        // THEN
        assertThat(result.id()).isEqualTo(todo.id());
        assertThat(result.title()).isEqualTo(todoUpdate.title());
        assertThat(result.completed()).isEqualTo(todoUpdate.completed());
        assertThat(result.order()).isEqualTo(todoUpdate.order());
    }

    @Test
    @DisplayName("handle updating a todo with invalid information")
    void reject_todo_update_with_invalid_information() {
        // GIVEN
        final UUID id = UUID.randomUUID();
        final Todo todo = new Todo(id, "Do Laundry", false, 1);
        todoRepository.save(todo);

        // WHEN
        final TodoUpdate todoUpdate = new TodoUpdate("Do shopping", true, -87);
        final TodoResult actualTodo = updateTodo.update(id, todoUpdate);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isInvalid()).isTrue();
    }

    @Test
    @DisplayName("handle non-existing todo when update complete")
    void handle_non_existing_todo_when_update_complete() {
        // GIVEN
        final UUID id = UUID.randomUUID();

        // WHEN
        final TodoUpdate todoUpdate = new TodoUpdate("Do shopping", true, 1);
        final TodoResult actualTodo = updateTodo.update(id, todoUpdate);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isNotFound()).isTrue();
    }

    @Test
    @DisplayName("handle conflicting todo when update complete")
    void handle_conflicting_todo_when_update_complete() {
        // GIVEN
        final UUID todoId1 = UUID.randomUUID();
        final Todo todo1 = new Todo(todoId1, "Do Laundry", false, 1);
        final Todo todo2 = new Todo(UUID.randomUUID(), "Do Dishes", false, 2);
        todoRepository.save(todo1);
        todoRepository.save(todo2);

        // WHEN
        final TodoUpdate todoUpdate = new TodoUpdate("Do shopping", true, 2);
        final TodoResult actualTodo = updateTodo.update(todoId1, todoUpdate);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isConflict()).isTrue();
    }

    @Test
    @DisplayName("update a todo partially")
    void update_todo_partially() {
        // GIVEN
        final UUID id = UUID.randomUUID();
        final Todo todo = new Todo(id, "Do Laundry", false, 1);
        todoRepository.save(todo);

        // WHEN
        final TodoPartialUpdate todoPartialUpdate = new TodoPartialUpdate(Optional.of("Do shopping"), Optional.empty(), Optional.empty());
        final TodoResult result = updateTodo.update(id, todoPartialUpdate);

        // THEN
        assertThat(result.id()).isEqualTo(todo.id());
        assertThat(result.title()).isEqualTo(todoPartialUpdate.title().get());
        assertThat(result.completed()).isEqualTo(todo.completed());
        assertThat(result.order()).isEqualTo(todo.order());
    }

    @Test
    @DisplayName("handle updating partially a todo with invalid information")
    void reject_todo_partial_update_with_invalid_information() {
        // GIVEN
        final UUID id = UUID.randomUUID();
        final Todo todo = new Todo(id, "Do Laundry", false, 1);
        todoRepository.save(todo);

        // WHEN
        final TodoPartialUpdate todoPartialUpdate = new TodoPartialUpdate(Optional.of("Do shopping"), Optional.of(true), Optional.of(-1L));
        final TodoResult actualTodo = updateTodo.update(id, todoPartialUpdate);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isInvalid()).isTrue();
    }

    @Test
    @DisplayName("handle non-existing todo when update partial")
    void handle_non_existing_todo_when_update_partial() {
        // GIVEN
        final UUID id = UUID.randomUUID();

        // WHEN
        final TodoPartialUpdate todoPartialUpdate = new TodoPartialUpdate(Optional.of("Do shopping"), Optional.of(true), Optional.empty());
        final TodoResult actualTodo = updateTodo.update(id, todoPartialUpdate);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isNotFound()).isTrue();
    }

    @Test
    @DisplayName("handle conflicting todo when update partial")
    void handle_conflicting_todo_when_update_partial() {
        // GIVEN
        final UUID todoId1 = UUID.randomUUID();
        final Todo todo1 = new Todo(todoId1, "Do Laundry", false, 1);
        final Todo todo2 = new Todo(UUID.randomUUID(), "Do Dishes", false, 2);
        todoRepository.save(todo1);
        todoRepository.save(todo2);

        // WHEN
        final TodoPartialUpdate todoPartialUpdate = new TodoPartialUpdate(Optional.of("Do shopping"), Optional.empty(), Optional.of(2L));
        final TodoResult actualTodo = updateTodo.update(todoId1, todoPartialUpdate);

        // THEN
        assertThat(actualTodo).isNull();
        assertThat(todoPresenter.isConflict()).isTrue();
    }
}