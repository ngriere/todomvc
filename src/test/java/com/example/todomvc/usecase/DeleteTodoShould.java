package com.example.todomvc.usecase;

import com.example.todomvc.adapter.InMemoryTodoPresenter;
import com.example.todomvc.adapter.InMemoryTodoRepository;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.usecase.port.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class DeleteTodoShould {
    private TodoRepository todoRepository;
    private InMemoryTodoPresenter todoPresenter;
    private DeleteTodo deleteTodo;

    @BeforeEach
    public void setUp() {
        todoRepository = new InMemoryTodoRepository();
        todoPresenter = new InMemoryTodoPresenter();
        deleteTodo = new DeleteTodo(todoRepository, todoPresenter);
        todoPresenter.clean();
    }

    @Test
    @DisplayName("delete a todo")
    void delete_todo() {
        // GIVEN
        final UUID id = UUID.randomUUID();
        final Todo todo = new Todo(id, "Do Laundry", false, 1);
        todoRepository.save(todo);
        assertThat(todoRepository.findAll()).hasSize(1);

        // WHEN
        deleteTodo.delete(id);

        // THEN
        assertThat(todoRepository.findAll()).isEmpty();
    }

    @Test
    @DisplayName("handle non existing todo")
    void handle_non_existing_todo() {
        // GIVEN
        final UUID id = UUID.randomUUID();

        // WHEN
        deleteTodo.delete(id);

        // THEN
        assertThat(todoPresenter.isNotFound()).isTrue();
    }

}