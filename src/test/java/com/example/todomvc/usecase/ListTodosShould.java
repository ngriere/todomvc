package com.example.todomvc.usecase;

import com.example.todomvc.adapter.InMemoryTodoPresenter;
import com.example.todomvc.adapter.InMemoryTodoRepository;
import com.example.todomvc.domain.Todo;
import com.example.todomvc.presenter.model.TodoResult;
import com.example.todomvc.usecase.port.TodoPresenter;
import com.example.todomvc.usecase.port.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class ListTodosShould {

    private TodoRepository todoRepository;
    private ListTodos listTodos;

    @BeforeEach
    public void setUp() {
        todoRepository = new InMemoryTodoRepository();
        TodoPresenter todoPresenter = new InMemoryTodoPresenter();
        listTodos = new ListTodos(todoRepository, todoPresenter);
    }

    @Test
    @DisplayName("list todos in order")
    void list_todos() {
        // GIVEN
        final UUID todoId1 = UUID.randomUUID();
        final Todo todo1 = new Todo(todoId1, "Do Laundry", false, 2);
        final Todo todo2 = new Todo(UUID.randomUUID(), "Do Dishes", false, 1);
        todoRepository.save(todo1);
        todoRepository.save(todo2);

        // WHEN
        final List<TodoResult> todoList = listTodos.list();

        // THEN
        assertThat(todoList).hasSize(2);
        assertThat(todoList.get(0).id()).isEqualTo(todo2.id());
        assertThat(todoList.get(1).id()).isEqualTo(todo1.id());
    }
}